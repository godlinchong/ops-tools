import paramiko


def ssh(hostname, username, password, newpassword, port):
    try:
        ssh = paramiko.SSHClient()
        ssh.load_system_host_keys()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(hostname=hostname, username=username, password=password, timeout=5)
        # stdin, stdout, stderr = ssh.exec_command("cat /etc/passwd")
        stdin, stdout, stderr = ssh.exec_command('echo "%s"|passwd --stdin admin'%newpassword)
        print stdout.read()
        #print hostname+" ok!"
        ssh.close()
    except Exception as e:
        print hostname + ":" + str(e)


if __name__ == '__main__':
    with open('service.txt', 'r') as f:
        for i in f:
            L = i.split()
            print L
            hostname = L[0]
            username = L[1]
            password = L[2]
            newpassword = L[3]
            port = 22
            ssh(hostname, username, password, newpassword, port)
