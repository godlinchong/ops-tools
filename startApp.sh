##on-prem
export JAVA_HOME="/usr/lib/jvm/jdk1.8.0_162"
export APP_HOME="/data/myna/rdc-segment-test/"
export APP_MAINCLASS="com.thomsonreuters.segment.boot.ProcessorStartApp";
export CLASSPATH=$APP_HOME/classes;

    for i in "$APP_HOME"/lib/*.jar; do
       CLASSPATH="$CLASSPATH":"$i"
    done

    #JAVA_OPTS="-Xms256m -Xmx32768m"
    JAVA_OPTS="-Xms2048m -Xmx2048m -Dcom.sun.management.jmxremote.port=3009 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false"
 
checkpid() {
       #javaps=`$JAVA_HOME/bin/jps -l | grep $APP_MAINCLASS`
       javaps=`ps -ef|grep $APP_MAINCLASS |grep -v "grep"|grep -v "bash"|awk '{print $2}'`
       if [ -n "$javaps" ]; then
          psid=`echo $javaps | awk '{print $1}'`
       else
          psid=0
       fi
    }

 start() {
       checkpid

       if [ $psid -ne 0 ]; then
          echo "================================"
          echo "warn: $APP_MAINCLASS already started! (pid=$psid)"
          echo "================================"
       else
          echo -n "Starting $APP_MAINCLASS ..."
          JAVA_CMD="nohup $JAVA_HOME/bin/java $JAVA_OPTS -classpath $CLASSPATH $APP_MAINCLASS 2>&1 &"
          eval $JAVA_CMD
          checkpid
          if [ $psid -ne 0 ]; then
             echo "(pid=$psid) [OK]"
          else
             echo "[Failed]"
          fi
       fi
    }



 stop() {
       checkpid

       if [ $psid -ne 0 ]; then
          echo -n "Stopping $APP_MAINCLASS ...(pid=$psid) "
          kill -9 $psid
          if [ $? -eq 0 ]; then
             echo "[OK]"
          else
             echo "[Failed]"
          fi

          checkpid
          if [ $psid -ne 0 ]; then
             stop
          fi
       else
          echo "================================"
          echo "warn: $APP_MAINCLASS is not running"
          echo "================================"
       fi
    }

case "$1" in
'start')
     start
     ;;
'stop')
     stop
     ;;
*)
echo "Usage: $0 {start|stop}"
esac
exit 0